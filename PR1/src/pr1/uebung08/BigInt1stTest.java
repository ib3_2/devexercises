package pr1.uebung08;

import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class BigInt1stTest {

	@Test
	public void demo() {
		BigInt zahl1 = new BigInt("23");
		BigInt zahl2 = new BigInt("45");

		zahl1.add(zahl2);

		assertEquals("68", zahl1.toString());
		assertEquals("45", zahl2.toString());

		assertEquals(6, zahl1.getDigits()[0]);
		assertEquals(8, zahl1.getDigits()[1]);

		assertEquals(2, zahl1.length());

		assertFalse(zahl1.equals(zahl2));
		assertTrue(zahl1.equals(new BigInt("68")));
	}

	@Test
	public void testSimple() {
		BigInt num1 = new BigInt("9000000000001");
		BigInt num2 = new BigInt("1000000000009");

		num1.add(num2);

		assertEquals("10000000000010", num1.toString());
		int[] expectedArray = new int[] { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 };
		assertTrue(Arrays.toString(expectedArray) + " vs " + Arrays.toString(num1.getDigits()),
				Arrays.equals(expectedArray, num1.getDigits()));
		assertEquals(14, num1.length());
		assertTrue(num1.equals(new BigInt("10000000000010")));
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void bigIntConstructorTest() {
		BigInt errorInt = new BigInt("sd");
	}
	
	@Test(expected = pr.MakeItSimple.PRException.class)
	public void bigIntConstructorTest2() {
		BigInt errorInt = new BigInt("3sd");
	}


}