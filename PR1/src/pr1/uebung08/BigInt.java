package pr1.uebung08;

import static pr.MakeItSimple.*;

//Teilaufgabe c) ist in der Klasse "BigInt50".

public class BigInt {

	public static void main(String[] args) {

		BigInt number1 = new BigInt("23");
		BigInt number2 = new BigInt("45");

		number1.add(number2);

		for (int i : number1.numberArray) {
			print(i + " ");
		}
		println("");
		for (int i : number2.numberArray) {
			print(i + " ");
		}
		println("");

		println(number1.toString());
		println(number1.getDigits()[0] + " & " + number1.getDigits()[1]);
		println(number1.equals(new BigInt("68")));
		println(number1.length());

	}

	private int[] numberArray;

	public BigInt(String number) {
		int arrayLenght = 0;
		for (int i = 0; i < number.length(); i++) {
			if ('0' <= number.charAt(i) && number.charAt(i) <= '9') {
				arrayLenght++;
			} else {
				throw new PRException("Fehler: Keine oder ungültige Zahl!");
			}
		}

		numberArray = new int[arrayLenght];

		setNumber(number);
	}

	private void setNumber(String number) {
		for (int i = 0; i < number.length(); i++) {
			if (number.charAt(i) == '0') {
				numberArray[i] = 0;
			} else if (number.charAt(i) == '1') {
				numberArray[i] = 1;
			} else if (number.charAt(i) == '2') {
				numberArray[i] = 2;
			} else if (number.charAt(i) == '3') {
				numberArray[i] = 3;
			} else if (number.charAt(i) == '4') {
				numberArray[i] = 4;
			} else if (number.charAt(i) == '5') {
				numberArray[i] = 5;
			} else if (number.charAt(i) == '6') {
				numberArray[i] = 6;
			} else if (number.charAt(i) == '7') {
				numberArray[i] = 7;
			} else if (number.charAt(i) == '8') {
				numberArray[i] = 8;
			} else if (number.charAt(i) == '9') {
				numberArray[i] = 9;
			}
		}
	}

	void add(BigInt number) {
		long mainNumber = getLongIntegerFromIntegerArray(numberArray);
		long numberToAdd = getLongIntegerFromIntegerArray(number.numberArray);

		long sum = mainNumber + numberToAdd;

		int newArrayLength = getNewArrayLengthForBigInt(sum);
		numberArray = new int[newArrayLength];
		for (int i = numberArray.length - 1; i >= 0; i--) {
			numberArray[i] = (int) (sum % 10);
			sum = sum / 10;
		}
	}

	private long getLongIntegerFromIntegerArray(int[] integerArray) {
		long mainNumber = 0;
		long multiplier = 1;
		for (int i = integerArray.length - 1; i >= 0; i--) {
			mainNumber = mainNumber + (integerArray[i] * multiplier);
			multiplier = multiplier * 10;
		}
		return mainNumber;
	}

	private int getNewArrayLengthForBigInt(long sum) {
		int newArrayLength = -1;

		long multiplier;
		long numberSizeChecker = 0;
		multiplier = 1;
		while (sum >= numberSizeChecker) {
			newArrayLength++;
			numberSizeChecker = 1 * multiplier;
			multiplier = multiplier * 10;
		}

		return newArrayLength;
	}

	public String toString() {
		String numberAsString = "";

		for (int i : numberArray) {
			numberAsString = numberAsString + i;
		}

		return numberAsString;
	}

	public int[] getDigits() {
		return numberArray;
	}

	public int length() {
		return numberArray.length;
	}

	public boolean equals(BigInt number) {
		boolean isEqual = false;

		if (number.numberArray.length == numberArray.length) {
			for (int i = 0; i < numberArray.length; i++) {
				if (number.numberArray[i] == numberArray[i]) {
					isEqual = true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}

		return isEqual;
	}
}
