package pr1.uebung08;

import static pr.MakeItSimple.*;

/*	Dies ist die Klasse BigInt, wenn die Arraylänge immer 50 haben soll.
 * 	Veränderungen im Quellcode werden mit einem "<--" markiert.
 */

public class BigInt50 {

	public static void main(String[] args) {

		BigInt50 number1 = new BigInt50("23");
		BigInt50 number2 = new BigInt50("45");

		number1.add(number2);

		for (int i : number1.numberArray) {
			print(i + " ");
		}
		println("");
		for (int i : number2.numberArray) {
			print(i + " ");
		}
		println("");

		println(number1.toString());
		println(number1.getDigits()[0] + "&" + number1.getDigits()[1]);
		println(number1.equals(new BigInt50("68")));
		println(number1.length());

	}

	private int[] numberArray;

	public BigInt50(String number) {
		// <--
		numberArray = new int[50]; // <--

		getNumberFromString(number);
	}

	private void getNumberFromString(String number) {
		for (int i = 0; i < number.length(); i++) {
			if (number.charAt(i) == '0') {
				numberArray[50 - number.length() + i] = 0; // <--
			} else if (number.charAt(i) == '1') {
				numberArray[50 - number.length() + i] = 1; // <--
			} else if (number.charAt(i) == '2') {
				numberArray[50 - number.length() + i] = 2; // <--
			} else if (number.charAt(i) == '3') {
				numberArray[50 - number.length() + i] = 3; // <--
			} else if (number.charAt(i) == '4') {
				numberArray[50 - number.length() + i] = 4; // <--
			} else if (number.charAt(i) == '5') {
				numberArray[50 - number.length() + i] = 5; // <--
			} else if (number.charAt(i) == '6') {
				numberArray[50 - number.length() + i] = 6; // <--
			} else if (number.charAt(i) == '7') {
				numberArray[50 - number.length() + i] = 7; // <--
			} else if (number.charAt(i) == '8') {
				numberArray[50 - number.length() + i] = 8; // <--
			} else if (number.charAt(i) == '9') {
				numberArray[50 - number.length() + i] = 9; // <--
			}
		}
	}

	public String toString() {
		String numberAsString = "";

		for (int i : numberArray) {
			numberAsString = numberAsString + i;
		}

		return numberAsString;
	}

	void add(BigInt50 number) {
		long mainNumber = getLongIntegerFromIntegerArray(numberArray);
		long numberToAdd = getLongIntegerFromIntegerArray(number.numberArray);

		long sum = mainNumber + numberToAdd;

		numberArray = setNumberToArray(sum); // <--
	}

	public int[] getDigits() {

		return numberArray;
	}

	public int length() {

		return numberArray.length;
	}

	public boolean equals(BigInt50 number) {
		boolean isEqual = false;

		// Wenn die Nullen vor der eigentlichen Zahl behalten werden sollen,
		// können die nächsten zwei Zeilen gelöscht werden
		long numberAsLong = getLongIntegerFromIntegerArray(number.numberArray);
		number.numberArray = setNumberToArray(numberAsLong);

		if (number.numberArray.length == numberArray.length) {
			for (int i = 0; i < numberArray.length; i++) {
				if (number.numberArray[i] == numberArray[i]) {
					isEqual = true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}

		return isEqual;
	}

	private long getLongIntegerFromIntegerArray(int[] integerArray) {
		long mainNumber = 0;
		long multiplier = 1;
		for (int i = integerArray.length - 1; i >= 0; i--) {
			mainNumber = mainNumber + (integerArray[i] * multiplier);
			multiplier = multiplier * 10;
		}
		return mainNumber;
	}

	private int[] setNumberToArray(long number) {
		int newArrayLength = getNewArrayLengthForBigInt(number);
		int[] array = new int[newArrayLength];
		// Wenn die Nullen vor der eigentlichen Zahl behalten werden sollen,
		// können die zwei vorherigen Zeilen gelöscht und "array" mit
		// "numberArray = new int[50]" ersetzt werden. Rückgabetyp wäre dann
		// void.
		for (int i = array.length - 1; i >= 0; i--) {
			array[i] = (int) (number % 10);
			number = number / 10;
		}

		return array;
	}

	// Wenn die Nullen vor der eigentlichen Zahl behalten werden sollen, kann
	// diese Methoge ebenfalls gelöscht werden.
	private int getNewArrayLengthForBigInt(long number) {
		int newArrayLength = -1;

		long multiplier;
		long numberSizeChecker = 0;
		multiplier = 1;
		while (number >= numberSizeChecker) {
			newArrayLength++;
			numberSizeChecker = 1 * multiplier;
			multiplier = multiplier * 10;
		}

		return newArrayLength;
	}
}