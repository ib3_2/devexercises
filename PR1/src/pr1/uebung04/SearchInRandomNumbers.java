package pr1.uebung04;

import static pr.MakeItSimple.*;

public class SearchInRandomNumbers {

	public static void main(String[] args) {

		print("geben sie die länge des arrays an :");
		int numberCount = readInt();
		print("geben sie die gesuchte Zahl ein: ");
		int numberToSearch = readInt();

		int[] generatedNumbers = generate(numberCount);
		int[] searchedNumberArray = searchAll(generatedNumbers, numberToSearch);

		if (searchedNumberArray.length != 0) {
			int index = 0;
			while (index < searchedNumberArray.length) {
				print(searchedNumberArray[index] + " ");
				index++;
			}
		} else {
			println("Zahl nicht in Zufallsarray gefunden.");
		}

		int lastPosition = searchLast(generatedNumbers, numberToSearch);
		println("Letzte Postion im Array: " + lastPosition);

	}

	public static int[] generate(int numberCount) {
		int[] zufallsZahl;
		if (numberCount >= 0) {
			int index = 0;
			zufallsZahl = new int[numberCount];
			while (index < numberCount) {
				zufallsZahl[index] = (int) (Math.random() * 1000) + 1;
				index++;
			}
		} else {
			throw new PRException("Zahl ungültig. (Zahl < 0)");

		}

		return zufallsZahl;
	}

	public static int[] searchAll(int[] generatedNumbers, int numberToSearch) {
		int index = 0;
		int zaehler = 0;

		while (index < generatedNumbers.length) {
			if (generatedNumbers[index] == numberToSearch) {
				zaehler++;
			}
			index++;
		}
		int[] searchedNumberArray;
		if (zaehler != 0) {
			searchedNumberArray = new int[zaehler];
			zaehler = 0;
			index = 0;
			while (index < generatedNumbers.length) {
				if (generatedNumbers[index] == numberToSearch) {
					searchedNumberArray[zaehler] = index;
					zaehler++;
				}
				index++;
			}
		} else {
			searchedNumberArray = new int[0];
		}

		return searchedNumberArray;
	}

	public static int searchLast(int[] generatedNumbers, int numberToSearch) {
		int lastPosition = 0;
		boolean hasChanged = false;
		int index = 0;
		while (index < generatedNumbers.length) {
			if (generatedNumbers[index] == numberToSearch) {
				lastPosition = index;
				hasChanged = true;
			}
			index++;
		}

		if (hasChanged == false) {
			throw new PRException("Zahl nicht gefunden");
		}

		return lastPosition;
	}
}
