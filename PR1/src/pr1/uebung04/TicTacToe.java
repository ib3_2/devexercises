package pr1.uebung04;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TicTacToe extends JFrame {

	private static final String EMPTY = "";
	private static final String CROSS = "X";
	private static final String CIRCLE = "O";
	private String currentSymbol = CROSS;
	private JButton[][] fields;

	TicTacToe() {
		super("Tic Tac Toe");
		setLayout(new GridLayout(3, 3));
		fields = new JButton[3][3];
		for (int row = 0; row < fields.length; row++) {
			for (int col = 0; col < fields[row].length; col++) {
				fields[row][col] = new JButton(EMPTY);
				fields[row][col].setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 36));
				fields[row][col].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						((JButton) e.getSource()).setText(currentSymbol);
						if (success(fields[0][0].getText(), fields[0][1].getText(), fields[0][2].getText(),
								fields[1][0].getText(), fields[1][1].getText(), fields[1][2].getText(),
								fields[2][0].getText(), fields[2][1].getText(), fields[2][2].getText())) {
							JOptionPane.showMessageDialog(TicTacToe.this,
									"Spieler '" + currentSymbol + "' hat gewonnen", "Gewonnen!",
									JOptionPane.INFORMATION_MESSAGE);
							System.exit(0);
						} else if (draw(fields[0][0].getText(), fields[0][1].getText(), fields[0][2].getText(),
								fields[1][0].getText(), fields[1][1].getText(), fields[1][2].getText(),
								fields[2][0].getText(), fields[2][1].getText(), fields[2][2].getText())) {
							JOptionPane.showMessageDialog(TicTacToe.this, "Das Spiel endete leider unentschieden",
									"Unentschieden!", JOptionPane.INFORMATION_MESSAGE);
							System.exit(0);
						} else {
							toggleSymbol();
						}
					}
				});
				add(fields[row][col]);
			}
		}

		setSize(300, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	void toggleSymbol() {
		if (currentSymbol.equals(CROSS))
			currentSymbol = CIRCLE;
		else
			currentSymbol = CROSS;
	}

	static boolean success(String topLeft, String topCenter, String topRight, String middleLeft, String middleCenter,
			String middleRight, String bottomLeft, String bottomCenter, String bottomRight) {

		boolean success = false;

		String xWinCondition = CROSS + CROSS + CROSS;
		String oWinCondition = CIRCLE + CIRCLE + CIRCLE;

		/*
		 * Es werden zu Erst die Strings für die jeweilige
		 * Zeilen-und-Spalten-Kombination gebildet. Diese werden im darauf
		 * folgenden boolean, z.B. isLeftHorizontal, mit den Gewinnbedinungen
		 * verglichen. Wenn der String gleich xWinConition oder oWinCondition
		 * ist, dann wird der Boolean wahr.
		 */
		String leftHorizontal = topLeft + middleLeft + bottomLeft;
		boolean isLeftHorizontal = leftHorizontal.equals(xWinCondition) || leftHorizontal.equals(oWinCondition);

		String centerHorizontal = topCenter + middleCenter + bottomCenter;
		boolean isCenterHorizontal = centerHorizontal.equals(xWinCondition) || centerHorizontal.equals(oWinCondition);

		String rightHorizontal = topRight + middleRight + bottomRight;
		boolean isRightHorizontal = rightHorizontal.equals(xWinCondition) || rightHorizontal.equals(oWinCondition);

		String topVertical = topLeft + topCenter + topRight;
		boolean isTopVertical = topVertical.equals(xWinCondition) || topVertical.equals(oWinCondition);

		String middleVertical = middleLeft + middleCenter + middleRight;
		boolean isMiddleVertical = middleVertical.equals(xWinCondition) || middleVertical.equals(oWinCondition);

		String bottomVertical = bottomLeft + bottomCenter + bottomRight;
		boolean isBottomVertical = bottomVertical.equals(xWinCondition) || bottomVertical.equals(oWinCondition);

		String leftDiagonal = topLeft + middleCenter + bottomRight;
		boolean isLeftDiagonal = leftDiagonal.equals(xWinCondition) || leftDiagonal.equals(oWinCondition);

		String rightDiagonal = topRight + middleCenter + bottomLeft;
		boolean isRightDiagonal = rightDiagonal.equals(xWinCondition) || rightDiagonal.equals(oWinCondition);

		/*
		 * Die boolische Variable verknüpft alle vorherigen bool's zu einem
		 * einzigem bool. Dies könnte man auch direkt in der if - Bedinung
		 * machen, allerdings würde die Bedinung, wie man sieht, sehr lang
		 * werden und die Verständlichkeit beeinträchtigen.
		 */
		boolean isWin = isLeftHorizontal || isCenterHorizontal || isRightHorizontal || isTopVertical || isMiddleVertical
				|| isBottomVertical || isLeftDiagonal || isRightDiagonal;

		if (isWin) {
			success = true;
		}

		return success;
	}

	static boolean draw(String topLeft, String topCenter, String topRight, String middleLeft, String middleCenter,
			String middleRight, String bottomLeft, String bottomCenter, String bottomRight) {

		boolean draw = false;

		// Man könnte die einzelnen Strings auch direkt zusammenfassen
		// allerdings leidet die Sauberkeit des Codes darunter.
		String topRow = topLeft + topCenter + topRight;
		String middleRow = middleLeft + middleCenter + middleRight;
		String bottomRow = bottomLeft + bottomCenter + bottomRight;

		String entireBoard = topRow + middleRow + bottomRow;

		// String.length() gibt die Länge eines Strings als Integer an, also wie
		// viele Zeichen ein String besitzt.
		boolean success = success(topLeft, topCenter, topRight, middleLeft, middleCenter, middleRight, bottomLeft,
				bottomCenter, bottomRight);
		if (entireBoard.length() == 9 && success == false) {
			draw = true;
		}

		return draw;
	}

	public static void main(String[] args) {
		new TicTacToe().setVisible(true);
	}

}