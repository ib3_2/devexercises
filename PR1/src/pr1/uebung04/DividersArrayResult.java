package pr1.uebung04;

import static pr.MakeItSimple.*;

public class DividersArrayResult {

	public static void main(String[] args) {

		println("Please enter a number: ");
		int userInput = readInt();

		boolean isPrime = isPrime(userInput);
		if (isPrime) {
			println("ist Prime");
		}
		if (userInput > 0) {
			int divisor = userInput; //

			int[] dividers = calculateDividers(divisor);

			int counter = 0;
			while (counter < 500 && dividers[counter] != 0) {
				print(dividers[counter] + " ");
				counter++;
			}
		} else {
			println("User input is invalid.");
		}
	}

	/**
	 * This method will calculate the dividers of a number.
	 * 
	 * @param divisor
	 * @return Array of dividers
	 */
	public static int[] calculateDividers(int divisor) {
		int[] dividers = new int[500];

		int divider = 1;
		int indexCounter = 0;
		while (divider <= 500) {
			if (divisor % divider == 0) {
				dividers[indexCounter] = divider;
				indexCounter++;
			}
			divider++;
		}

		return dividers;
	}

	static boolean isPrime(int n) {
		boolean isPrime = false;
		if ((n % 2 == 1 && n >= 2) || n == 2) {
			isPrime = true;
		}
		return isPrime;
	}
}
