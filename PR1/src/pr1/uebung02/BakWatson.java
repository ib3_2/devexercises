package pr1.uebung02;

import static pr.MakeItSimple.*;
public class BakWatson {

	public static void main(String[] args) {
		
		// Alle nötigen Variablen deklaiert 
		
		double a, g, m ,l, e, ggkw;
		String geschlecht; 
		//Einführungstext
		println("Dieses Programm bestimmt die Blutkonzentration, sprich die Menge von Alkohol im Blut. Bitte geben sie die Werte ohne Einheitskürzel ein ");
		println("Bitte geben sie den passenden Reduktions-/Verteilungsfaktor für ihr Geschlecht/Altersklasse an ");
		//Nach den einzelnen Daten gefragt. 
		print("Bitte geben sie ihr Geschlecht an (maennlich oder weiblich) ");
		geschlecht = readString();
		
		print("Bitte geben sie ihr Gewicht in kg an ");
		m = readDouble();
		
		print("Bitte geben sie ihre Körpergröße in cm an ");
		g = readDouble(); 
		
		print("Bitte geben sie ihr Alter an ");
		a = readDouble(); 
		
		print("Bitte geben sie ihre Menge vom Alkoholgetränk in Liter ein (Bsp.: 0.5) ");
		l = readDouble();
		l = l * 1000; 
		
		print("Bitte geben sie den Alkoholvolumenanteil vom Getränk ein (Bsp.: Bier = 0.05) ");
		e = readDouble(); 
		
		//If abfrage nachdem Geschlecht, da die verschiedenen Kreterien zu viele Unterschiede in den Faktioren haben
		//Man muss hierbei equals zum vergleichen benutzen, da der Operator " == " nicht für Strings geeignet ist, da sie nicht den Inhalt bzw die Länge des Strings prüft
		if(geschlecht.equals("maennlich")){
			ggkw = 2.447- 0.09516 * a +0.1074 * g + 0.3362 * m;
			println( "Sie haben ungefähr eine BAK zwischen "  +  (((l * 0.8 * e) *0.8) / (1.055 * ggkw)  - (((l * 0.8 * e) *0.8) / (1.055 * ggkw) *0.1)) + "‰ und   "  +  (((l * 0.8 * e) *0.8) / (1.055 * ggkw)  - (((l * 0.8 * e) *0.8) / (1.055 * ggkw) *0.3)) );
		}
		else if (geschlecht.equals("weiblich")){
			ggkw = -2.097 + 0.1069 * g + 0.2466 * m;
			println( "Sie haben ungefähr eine BAK zwischen "  +  (((l * 0.8 * e) *0.8) / (1.055 * ggkw)  - (((l * 0.8 * e) *0.8) / (1.055 * ggkw) *0.1)) + "‰ und   "  +  (((l * 0.8 * e) *0.8) / (1.055 * ggkw)  - (((l * 0.8 * e) *0.8) / (1.055 * ggkw) *0.3)) );
		}
		else{
			println("Ungültige Eingabe");
		}
		
		 
		
		
	}

}
