package pr1.uebung02;

import static pr.MakeItSimple.*; 

public class BkaWidmark {

	// Alle nötigen Variablen deklaiert
	public static void main(String[] args) {
		double r, m ,l, e; 		
		
		//Einführungstext 
		println("Dieses Programm bestimmt die Blutkonzentration, sprich die Menge von Alkohol im Blut. Bitte geben sie die Werte ohne Einheitskürzel ein ");
		println("Bitte geben sie den passenden Reduktions-/Verteilungsfaktor für ihr Geschlecht/Altersklasse an ");
		
		//Den Custommer den Wert hineinschreiben lassen, anstatt eine If Verzweigung
		println("Männer 0.7 ");
		println("Frauen/Jugendliche 0.6 ");
		println("Säuglinge/Kleinkinder 0.8 ");
		//DIe verschiedenen Werte werden in die entsprechenden Variablen gegeben 
		r = readDouble();
		
		print("Bitte geben sie ihr Gewicht in kg an ");
		m = readDouble(); 
		
		print("Bitte geben sie ihre Menge vom Alkoholgetränk in Liter ein (Bsp.: 0.5) ");
		l = readDouble();
		l = l * 1000; 
		
		print("Bitte geben sie den Alkoholvolumenanteil vom Getränk ein (Bsp.: Bier = 0.05) ");
		e = readDouble(); 
		
		
		//Endausgabe mit den Werten und der Rechnung
		println("Sie haben ungefähr eine BAK zwischen " +  (((l * 0.8 * e)/(m * r))-(((l * 0.8 * e)/(m * r)) *0.1)) + "  und  " + (((l * 0.8 * e)/(m * r))-(((l * 0.8 * e)/(m * r)) *0.3))); 
	}

}
