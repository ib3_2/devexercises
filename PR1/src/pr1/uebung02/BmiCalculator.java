//Rosario Vella Daniel Korus 12.10.2017
package pr1.uebung02;

import static pr.MakeItSimple.*;

public class BmiCalculator {

	public static void main(String[] args) {
		
		
		//Geschlecht Abfrage mit Schleife wenn unsinnig
		//boolean correct;
		//do {
			println("Sind Sie ein Mann oder eine Frau?");
			
		//	correct = true;
		//verschiedene Schreibweisen
		/*switch (geschlecht) {
		
		case "Mann":
			 println("Sie sind ein Mann");
			 break;
		case "mann":
			 println("Sie sind ein Mann");
			 break;
		case "Frau":
			 println("Sie sind eine Frau");
			 break;
		case "frau":
			 println("Sie sind eine Frau");
			 break;
	    default:   	
	    	println(" Bitte geben sie Mann,mann,Frau oder frau ein");
	    	correct = false;
			break;
					}
		*/
			String geschlecht = readString();

			if(geschlecht.equals("Mann")|| geschlecht.equals("mann")){
				
				println("Sie sind ein Mann");
				
			}
			if(geschlecht.equals("Frau")|| geschlecht.equals("frau")){
				
				println("Sie sind eine Frau");
			}
			
	//	} while (!correct);
		//KoerperGewichtAbfrage
		println("Geben Sie ihr Körpergewicht ein");
		double koerperGewicht = readDouble();
		//beschränkung der eingabe des koerpergewichts
		if(koerperGewicht >= 30 &&koerperGewicht <= 300) {
			//GroeseAbfrage
			println("Geben Sie ihre Größe in Meter an Bsp.: '1.8'");
			double groese = readDouble();
		//beschränkung der abfrage der körpergroese	
		if(groese >= 1.2 && groese <=2.5 ) {
				
			
			
		    double	ergebniss = koerperGewicht / (groese * groese);
		    print("Ihm BMI ist: "); 
		    print(ergebniss);
			}
		//bei unrealistischen eingaben
		else{
			println("Der Bmi kann nur bei einer groese zwischwen 1.2m und 2.5m berechnet werden");
		}
			
		}
		//bei unrealistischen eingaben
		else {
			println("Der Bmi kann nur bei einem gewicht zwischen 30kg und 300kg berechnet werden." );
		}
			
		}

}
