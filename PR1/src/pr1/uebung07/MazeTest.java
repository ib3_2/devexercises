package pr1.uebung07;

import static org.junit.Assert.*;
import static pr.MakeItSimple.print;
import static pr.MakeItSimple.println;

import org.junit.Test;

public class MazeTest {

	char[][] LAB = new char[][] { 
		"##########".toCharArray(), 
		"#        #".toCharArray(), 
		"### #### #".toCharArray(),
		"#     ## #".toCharArray(), 
		"##### ## S".toCharArray(), 
		"#   # ####".toCharArray(),
		"# # #    #".toCharArray(), 
		"# # #### #".toCharArray(), 
		"# #      #".toCharArray(),
		"# ########".toCharArray(), };

	private void assertEqualsCharArray(char[][] expected, char[][] actual) {
		for (int row = 0; row < expected.length; row++)
			assertArrayEquals("Zeile " + row, expected[row], actual[row]);
	}

	@Test
	public void mazeExample() {

		Maze.findWay(LAB);

		for (int row = 0; row < LAB.length; row++) {
			for (int col = 0; col < LAB[0].length; col++)
				print("" + LAB[row][col]);
			println();
		}

		assertEqualsCharArray(new char[][] { 
			"##########".toCharArray(), 
			"#  ......#".toCharArray(),
			"###.####.#".toCharArray(), 
			"#  ...##.#".toCharArray(), 
			"#####.##..".toCharArray(),
			"#...#.####".toCharArray(), 
			"#.#.#....#".toCharArray(), 
			"#.#.####.#".toCharArray(),
			"#.#......#".toCharArray(), 
			"#E########".toCharArray() }, LAB);
	}

	@Test
	public void findStartPointTest() {
		
		int[] expectedStartPoint = { 4, 9 };

		int[] actualStartPoint = Maze.findStartPoint(LAB, 0, 0);

		for (int i = 0; i < expectedStartPoint.length; i++) {
			assertEquals(expectedStartPoint[i], actualStartPoint[i]);
		}
	}
	
	@Test
	public void findStartPointAtLeftUpperCornerTest(){
		char[][] lab = new char[][] { 
			"S#########".toCharArray(), 
			"#        #".toCharArray(), 
			"### #### #".toCharArray(),
			"#     ## #".toCharArray(), 
			"##### ## #".toCharArray(), 
			"#   # ####".toCharArray(),
			"# # #    #".toCharArray(), 
			"# # #### #".toCharArray(), 
			"# #      #".toCharArray(),
			"# ########".toCharArray(), };
		
		int[] expectedStartPoint = {0,0}; 
		
		int[] actualStartPoint = Maze.findStartPoint(lab, 0, 0);
		
		for (int i = 0; i < expectedStartPoint.length; i++) {
			assertEquals(expectedStartPoint[i], actualStartPoint[i]);
		}
	}
	
	@Test
	public void findStartPointAtRightUpperCornerTest(){
		char[][] lab = new char[][] { 
			"#########S".toCharArray(), 
			"#        #".toCharArray(), 
			"### #### #".toCharArray(),
			"#     ## #".toCharArray(), 
			"##### ## #".toCharArray(), 
			"#   # ####".toCharArray(),
			"# # #    #".toCharArray(), 
			"# # #### #".toCharArray(), 
			"# #      #".toCharArray(),
			"# ########".toCharArray(), };
		
		int[] expectedStartPoint = {0,9}; 
		
		int[] actualStartPoint = Maze.findStartPoint(lab, 0, 0);
		
		for (int i = 0; i < expectedStartPoint.length; i++) {
			assertEquals(expectedStartPoint[i], actualStartPoint[i]);
		}
	}
	
	@Test
	public void findStartPointAtLeftLowerCornerTest(){
		char[][] lab = new char[][] { 
			"##########".toCharArray(), 
			"#        #".toCharArray(), 
			"### #### #".toCharArray(),
			"#     ## #".toCharArray(), 
			"##### ## #".toCharArray(), 
			"#   # ####".toCharArray(),
			"# # #    #".toCharArray(), 
			"# # #### #".toCharArray(), 
			"# #      #".toCharArray(),
			"S ########".toCharArray(), };
		
		int[] expectedStartPoint = {9,0}; 
		
		int[] actualStartPoint = Maze.findStartPoint(lab, 0, 0);
		
		for (int i = 0; i < expectedStartPoint.length; i++) {
			assertEquals(expectedStartPoint[i], actualStartPoint[i]);
		}
	}
	
	@Test
	public void findStartPointAtRightLowerCornerTest(){
		char[][] lab = new char[][] { 
			"##########".toCharArray(), 
			"#        #".toCharArray(), 
			"### #### #".toCharArray(),
			"#     ## #".toCharArray(), 
			"##### ## #".toCharArray(), 
			"#   # ####".toCharArray(),
			"# # #    #".toCharArray(), 
			"# # #### #".toCharArray(), 
			"# #      #".toCharArray(),
			"# #######S".toCharArray(), };
		
		int[] expectedStartPoint = {9,9}; 
		
		int[] actualStartPoint = Maze.findStartPoint(lab, 0, 0);
		
		for (int i = 0; i < expectedStartPoint.length; i++) {
			assertEquals(expectedStartPoint[i], actualStartPoint[i]);
		}
	}

}