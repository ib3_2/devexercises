package pr1.uebung07;

import static pr.MakeItSimple.*;

/*
Es sind vollständige Tests zu jeder Methode vorhanden. 
Weiterhin können über die main-Methode, die anderen Methoden getestet werden.
 */
public class StringExtension {

	static char[][] ALPHABET = { { 'a', 'A' }, { 'b', 'B' }, { 'c', 'C' }, { 'd', 'D' }, { 'e', 'E' }, { 'f', 'F' },
			{ 'g', 'G' }, { 'h', 'H' }, { 'i', 'I' }, { 'j', 'J' }, { 'k', 'K' }, { 'l', 'L' }, { 'm', 'M' },
			{ 'n', 'N' }, { 'o', 'O' }, { 'p', 'P' }, { 'q', 'Q' }, { 'r', 'R' }, { 's', 'S' }, { 't', 'T' },
			{ 'u', 'U' }, { 'v', 'V' }, { 'w', 'W' }, { 'x', 'X' }, { 'y', 'Y' }, { 'z', 'Z' }, { 'ö', 'Ö' },
			{ 'ä', 'Ä' }, { 'ü', 'Ü' } };

	static int LOWER_CASE_LETTER = 0;
	static int UPPER_CASE_LETTER = 1;

	static void main(String[] args) {

	}

	// Start Teilaufgabe a)
	static String toUpper(String text) {
		String upperCaseText = "";

		char[] textCharArray = text.toCharArray();

		for (int i = 0; i < textCharArray.length; i++) {
			String tempText = upperCaseText;
			for (int j = 0; j < ALPHABET.length; j++) {
				if (textCharArray[i] == ALPHABET[j][LOWER_CASE_LETTER]) {
					tempText = tempText + ALPHABET[j][UPPER_CASE_LETTER];

					j = ALPHABET.length;
				}
			}
			if (tempText == upperCaseText) {
				upperCaseText = upperCaseText + textCharArray[i];
			} else {
				upperCaseText = tempText;
			}
		}

		return upperCaseText;
	}
	// Ende a)

	// Start Teilaufgabe b)
	static String toUpperRecursive(String text) {

		if (text.equals("")) {
			return text;
		}

		char upperCaseChar = getUpperCaseLetter(text.charAt(0), 0);
		String newText = deleteCharFromString(text, 1);

		if (0 < newText.length()) {
			return upperCaseChar + toUpperRecursive(newText);
		} else {
			return "" + upperCaseChar;
		}
	}

	static char getUpperCaseLetter(char charToChange, int alphabetDepth) {
		if (charToChange == ALPHABET[alphabetDepth][LOWER_CASE_LETTER]) {
			return ALPHABET[alphabetDepth][UPPER_CASE_LETTER];
		} else if (alphabetDepth < ALPHABET.length - 1) {
			return getUpperCaseLetter(charToChange, alphabetDepth + 1);
		} else {
			return charToChange;
		}
	}

	static String deleteCharFromString(String text, int stringDepth) {
		if (stringDepth >= text.length()) {
			return "";
		} else {
			return text.charAt(stringDepth) + deleteCharFromString(text, stringDepth + 1);
		}
	}
	// Ende b)

	// Start Teilaufgabe c)
	static int scan(String text, String needle) {

		int position = -1;
		char[] textCharArray = text.toCharArray();
		boolean positionNotFound = true;
		for (int i = 0; i < textCharArray.length && positionNotFound; i++) {
			String tempString = "";
			for (int j = 0; j < needle.length(); j++) {
				if (i + j < textCharArray.length) {
					tempString = tempString + textCharArray[i + j];
				}
			}
			if (tempString.equals(needle)) {
				position = i;
				positionNotFound = false;
			}
		}

		if (position == -1) {
			throw new PRException("Fehler: Wort nicht gefunden.");
		} else if (needle.equals("")) {
			throw new PRException("Fehler: Leerstring.");
		}

		return position;
	}

	static int scanRecursive(String text, String needle, int needleDepth, int stringDepth) {

		if (needle.equals(getTempString(needle, text, stringDepth, needleDepth))) {
			return stringDepth;
		} else if (stringDepth < text.length()) {
			return scanRecursive(text, needle, needleDepth, stringDepth + 1);
		} else {
			throw new PRException("Fehler: Wort nicht gefunden.");
		}

	}

	static String getTempString(String needle, String text, int stringDepth, int needleDepth) {
		char[] textCharArray = text.toCharArray();
		if (needleDepth < needle.length() && stringDepth + needleDepth < textCharArray.length) {
			return "" + textCharArray[stringDepth + needleDepth]
					+ getTempString(needle, text, stringDepth, needleDepth + 1);
		} else if (needle.equals("")) {
			throw new PRException("Fehler: Needle ist ein Leerstring.");
		} else {
			return "";
		}
	}
	// Ende c)

	// Start Teilaufgabe d)
	static String[] split(String text, char delimiter) {
		char[] textCharArray = text.toCharArray();

		int stringArrayLength = getStringArrayLength(delimiter, textCharArray);
		if (stringArrayLength == 1) {
			throw new PRException("Fehler: Delimiter nicht gefunden");
		}

		String[] splittedString = new String[stringArrayLength];
		int delimiterPosition = -1; // Imaginärer Delimiter vor dem ersten Wort
									// ((;)Hallo;...)
		for (int i = 0; i < splittedString.length; i++) {
			splittedString[i] = "";
			for (int j = delimiterPosition + 1; j < textCharArray.length; j++) {
				if (textCharArray[j] == delimiter) {
					delimiterPosition = j;
					j = textCharArray.length;
				} else {
					splittedString[i] = splittedString[i] + textCharArray[j];
				}
			}
		}

		return splittedString;
	}

	static int getStringArrayLength(char delimiter, char[] textCharArray) {
		int stringArrayIndex = 1;
		for (int i = 0; i < textCharArray.length; i++) {
			if (textCharArray[i] == delimiter && (i + 1) <= textCharArray.length) {
				stringArrayIndex = stringArrayIndex + 1;
			}
		}
		return stringArrayIndex;
	}
	// Ende d)

}
