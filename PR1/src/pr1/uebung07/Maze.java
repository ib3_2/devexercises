package pr1.uebung07;

import static pr.MakeItSimple.*;

public class Maze {

	public static void main(String[] args) {
		char[][] testMaze = { "#S#".toCharArray(), "# #".toCharArray(), "# #".toCharArray() };

	}

	static void findWay(char[][] field) {

		if (field.length < 3 || field[0].length < 3) {
			throw new PRException("Fehler: Ungültiges Array!");
		}

		int[] startPoint = findStartPoint(field, 0, 0);

		boolean findWayRecursive = findWayRecursive(field, startPoint[0], startPoint[1]);

		if (!findWayRecursive) {
			throw new PRException("Fehler: Kein Ausgang!");
		}
	}

	static int[] findStartPoint(char[][] field, int row, int col) {
		int[] startPoint = new int[2];

		if (field[row][col] == 'S' || field[row][col] == 's') {
			startPoint[0] = row;
			startPoint[1] = col;
			return startPoint;
		} else if (col < field.length - 1) {
			return findStartPoint(field, row, col + 1);
		} else {
			return findStartPoint(field, row + 1, col - (field.length - 1));
		}
	}

	static boolean findWayRecursive(char[][] field, int row, int col) {
		isEnd(field, row, col);
		if (field[row][col] == 'E') {
			if (field[row][col] == ' ') {
				field[row][col] = '.';
			}
			return true;
		}

		boolean isUpperBorder = row - 1 < 0;
		boolean isLeftBorder = col - 1 < 0;
		boolean isLowerBorder = row + 1 >= field.length;
		boolean isRightBorder = col + 1 >= field.length;

		boolean isUpperWay = false;
		boolean isLeftWay = false;
		boolean isLowerWay = false;
		boolean isRightWay = false;
		if (!isUpperBorder) {
			isUpperWay = field[row - 1][col] == ' ';
		}
		if (!isLeftBorder) {
			isLeftWay = field[row][col - 1] == ' ';
		}
		if (!isLowerBorder) {
			isLowerWay = field[row + 1][col] == ' ';
		}
		if (!isRightBorder) {
			isRightWay = field[row][col + 1] == ' ';
		}

		boolean foundWay = false;
		if (field[row][col] == 'S' || field[row][col] == ' ') {
			field[row][col] = '.';
		}
		if (isUpperWay && !foundWay) {
			foundWay = findWayRecursive(field, row - 1, col);
		}
		if (isLeftWay && !foundWay) {
			foundWay = findWayRecursive(field, row, col - 1);
		}
		if (isLowerWay && !foundWay) {
			foundWay = findWayRecursive(field, row + 1, col);
		}
		if (isRightWay && !foundWay) {
			foundWay = findWayRecursive(field, row, col + 1);
		}
		if (!foundWay) {
			field[row][col] = ' ';
		}
		return foundWay;

	}

	static boolean isEnd(char[][] field, int row, int col) {
		boolean isUpperBorder = row - 1 < 0 && field[row][col] != 'S';
		boolean isLeftBorder = col - 1 < 0 && field[row][col] != 'S';
		boolean isLowerBorder = row + 1 >= field.length && field[row][col] != 'S';
		boolean isRightBorder = col + 1 >= field.length && field[row][col] != 'S';

		boolean isEnd = false;
		if (isUpperBorder) {
			isEnd = field[row][col] == ' ';
			field[row][col] = 'E';
		}
		if (isLeftBorder) {
			isEnd = field[row][col] == ' ';
			field[row][col] = 'E';
		}
		if (isLowerBorder) {
			isEnd = field[row][col] == ' ';
			field[row][col] = 'E';
		}
		if (isRightBorder) {
			isEnd = field[row][col] == ' ';
			field[row][col] = 'E';
		}

		return isEnd;
	}

}
