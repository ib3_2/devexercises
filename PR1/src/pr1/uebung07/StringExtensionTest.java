package pr1.uebung07;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringExtensionTest {

	@Test
	public void toUpperTest() {
		String text = "Hallo, das ist ein Test!SCHöNäNTagüß?";

		String expectedText = "HALLO, DAS IST EIN TEST!SCHÖNÄNTAGÜß?";

		String actualText = StringExtension.toUpper(text);

		assertEquals(expectedText, actualText);
	}

	@Test
	public void toUpperCaseRecursiveMainTest() {
		String text = "Hallo, das ist ein Test!SCHöNäNTagü";

		String expectedText = "HALLO, DAS IST EIN TEST!SCHÖNÄNTAGÜ";

		String actualText = StringExtension.toUpperRecursive(text);

		assertEquals(expectedText, actualText);
	}

	@Test
	public void toUpperCaseRecursiveEmptyTextTest() {
		String text = "";

		assertEquals("", StringExtension.toUpperRecursive(text));
	}

	@Test
	public void toUpperCaseRecursiveCrazyTextTest() {
		String text = "H A l lo,,,, daaa s ist ein     Test! SChöönüünään Tag!!!11";

		String expectedText = "H A L LO,,,, DAAA S IST EIN     TEST! SCHÖÖNÜÜNÄÄN TAG!!!11";

		String actualText = StringExtension.toUpperRecursive(text);

		assertEquals(expectedText, actualText);
	}

	@Test
	public void getUpperCaseLetterTest() {
		char charToChange1 = 'b';
		char charToChange2 = ' ';
		char charToChange3 = '!';
		char charToChange4 = 'z';
		int alphabetDepth = 0;

		char expectedChar1 = 'B';
		char expectedChar2 = ' ';
		char expectedChar3 = '!';
		char expectedChar4 = 'Z';

		char actualChar1 = StringExtension.getUpperCaseLetter(charToChange1, alphabetDepth);
		char actualChar2 = StringExtension.getUpperCaseLetter(charToChange2, alphabetDepth);
		char actualChar3 = StringExtension.getUpperCaseLetter(charToChange3, alphabetDepth);
		char actualChar4 = StringExtension.getUpperCaseLetter(charToChange4, alphabetDepth);

		assertEquals(expectedChar1, actualChar1);
		assertEquals(expectedChar2, actualChar2);
		assertEquals(expectedChar3, actualChar3);
		assertEquals(expectedChar4, actualChar4);
	};

	@Test
	public void deleteCharFromStringMainTest() {
		String text1 = "aHaus";
		String text2 = "a";
		int stringDepth = 1;

		String expectedText1 = "Haus";
		String expectedText2 = "";

		String actualText1 = StringExtension.deleteCharFromString(text1, stringDepth);
		String actualText2 = StringExtension.deleteCharFromString(text2, stringDepth);

		assertEquals(expectedText1, actualText1);
		assertEquals(expectedText2, actualText2);

	}

	@Test
	public void deleteCharFromStringCrazyTextTest() {
		String text1 = "a !  H a u   !!!!s";
		String text2 = "a";
		int stringDepth = 1;

		String expectedText1 = " !  H a u   !!!!s";
		String expectedText2 = "";

		String actualText1 = StringExtension.deleteCharFromString(text1, stringDepth);
		String actualText2 = StringExtension.deleteCharFromString(text2, stringDepth);

		assertEquals(expectedText1, actualText1);
		assertEquals(expectedText2, actualText2);

	}

	@Test
	public void deleteCharFromStringEmptyTextTest() {
		String text = "";

		String expectedText = "";

		String actualText = StringExtension.deleteCharFromString(text, 1);

		assertEquals(expectedText, actualText);
	}

	@Test
	public void scanTest() {
		String text = "Hallo, wie geht das? Hallo.";
		String needle1 = "Hallo";
		String needle2 = "wie";
		String needle3 = "geht";
		String needle4 = "das?";

		int expectedPosition1 = 0;
		int expectedPosition2 = 7;
		int expectedPosition3 = 11;
		int expectedPosition4 = 16;

		int actualPosition1 = StringExtension.scan(text, needle1);
		int actualPosition2 = StringExtension.scan(text, needle2);
		int actualPosition3 = StringExtension.scan(text, needle3);
		int actualPosition4 = StringExtension.scan(text, needle4);

		assertEquals(expectedPosition1, actualPosition1);
		assertEquals(expectedPosition2, actualPosition2);
		assertEquals(expectedPosition3, actualPosition3);
		assertEquals(expectedPosition4, actualPosition4);
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void scanWordNotFoundTest() {
		StringExtension.scan("Hallo Welt!", "Du.");
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void scanWordNeedleIsEmptyTest() {
		StringExtension.scan("Hallo Welt!", "");
	}

	@Test
	public void scanRecursiveTest() {
		String text = "Hallo, wie geht das? Hallo.";
		String needle1 = "Hallo";
		String needle2 = "wie";
		String needle3 = "geht";
		String needle4 = "das?";

		int expectedPosition1 = 0;
		int expectedPosition2 = 7;
		int expectedPosition3 = 11;
		int expectedPosition4 = 16;

		int actualPosition1 = StringExtension.scanRecursive(text, needle1, 0, 0);
		int actualPosition2 = StringExtension.scanRecursive(text, needle2, 0, 0);
		int actualPosition3 = StringExtension.scanRecursive(text, needle3, 0, 0);
		int actualPosition4 = StringExtension.scanRecursive(text, needle4, 0, 0);

		assertEquals(expectedPosition1, actualPosition1);
		assertEquals(expectedPosition2, actualPosition2);
		assertEquals(expectedPosition3, actualPosition3);
		assertEquals(expectedPosition4, actualPosition4);
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void scanRecursiveWordNotFoundTest() {
		StringExtension.scanRecursive("Hallo Welt!", "Du.", 0, 0);
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void scanRecursiveWordNeedleIsEmptyTest() {
		StringExtension.scan("Hallo Welt!", "");
	}

	@Test
	public void splitTest() {
		String text = "hallo;wie;geht;es;dir";

		String[] expectedStringArray = { "hallo", "wie", "geht", "es", "dir" };

		String[] splittedText = StringExtension.split(text, ';');

		for (int i = 0; i < expectedStringArray.length; i++) {
			assertEquals(expectedStringArray[i], splittedText[i]);
		}
	}

	@Test
	public void splitWhitespacesTest() {
		String text = "    ;    ;    ; ;";

		String[] expectedStringArray = { "    ", "    ", "    ", " " };

		String[] splittedText = StringExtension.split(text, ';');

		for (int i = 0; i < expectedStringArray.length; i++) {
			assertEquals(expectedStringArray[i], splittedText[i]);
		}
	}

	@Test(expected = pr.MakeItSimple.PRException.class)
	public void splitDelimiterNotFoundTest() {
		StringExtension.split("Hall;o;wie;geht;es?", '.');
	}

	@Test
	public void getStringArrayLengthTest() {
		String text = "hallo;wie;geht;es;dir";
		char[] textCharArray = text.toCharArray();

		int expectedInt = 5;

		int actualInt = StringExtension.getStringArrayLength(';', textCharArray);

		assertEquals(expectedInt, actualInt);

	}

	@Test
	public void getStringArrayLengthWithEmptyElementsTest() {
		String text1 = "    ;    ;    ; ;";
		char[] textCharArray = text1.toCharArray();

		int expectedInt = 5;

		int actualInt = StringExtension.getStringArrayLength(';', textCharArray);

		assertEquals(expectedInt, actualInt);
		assertEquals(2, StringExtension.getStringArrayLength('a', "a".toCharArray()));

	}
}
