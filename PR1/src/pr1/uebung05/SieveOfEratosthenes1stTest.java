package pr1.uebung05;

import static org.junit.Assert.*;

import org.junit.Test;

public class SieveOfEratosthenes1stTest {

	@Test
	public void zeroIsNotPrime() {
		boolean[] primeMarkers = SieveOfEratosthenes.calculatePrimes(10);
		assertFalse(primeMarkers[0]);
	}

	@Test
	public void twoPrimesInARow() {
		boolean[] primeMarkers = SieveOfEratosthenes.calculatePrimes(3);
		assertEquals(4, primeMarkers.length);
		assertTrue(primeMarkers[2]);
		assertTrue(primeMarkers[3]);
	}

	@Test
	public void primesUntilHundred() {
		boolean[] primeMarkers = SieveOfEratosthenes.calculatePrimes(100);
		assertEquals(101, primeMarkers.length);
		assertTrue(primeMarkers[2]);
		assertTrue(primeMarkers[3]);
		assertTrue(primeMarkers[5]);
		assertTrue(primeMarkers[7]);
		assertTrue(primeMarkers[11]);
		assertTrue(primeMarkers[13]);
		assertTrue(primeMarkers[17]);
		assertTrue(primeMarkers[19]);
		assertTrue(primeMarkers[23]);
		assertTrue(primeMarkers[29]);
		assertTrue(primeMarkers[31]);
		assertTrue(primeMarkers[37]);
		assertTrue(primeMarkers[41]);
		assertTrue(primeMarkers[43]);
		assertTrue(primeMarkers[47]);
		assertTrue(primeMarkers[53]);
		assertTrue(primeMarkers[59]);
		assertTrue(primeMarkers[61]);
		assertTrue(primeMarkers[67]);
		assertTrue(primeMarkers[71]);
		assertTrue(primeMarkers[73]);
		assertTrue(primeMarkers[79]);
		assertTrue(primeMarkers[83]);
		assertTrue(primeMarkers[89]);
		assertTrue(primeMarkers[97]);

	}

}