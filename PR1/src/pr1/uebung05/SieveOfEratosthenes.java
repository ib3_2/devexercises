package pr1.uebung05;

import static pr.MakeItSimple.*;

public class SieveOfEratosthenes {

	public static void main(String[] args) {
		print("Please enter a number ");

		int n = readInt();

		if (n < 2) {	
			throw new PRException("Zahl kleiner als 2!");
		}

		boolean[] primes = calculatePrimes(n);

		for (int i = 0; i < primes.length; i++) {
			if (primes[i] == true) {
				print(i + " ");
			}
		}
	}

	public static boolean[] calculatePrimes(int n) {

		boolean[] primes = new boolean[n + 1];

		for (int i = 2; i <= n; i++) {
			primes[i] = true;
		}

		for (int i = 2; i <= n; i++) {
			if (primes[i] == true) {
				for (int j = i + i; j <= n; j = j + i) {
					primes[j] = false;
				}
			}
		}

		return primes;
	}

}
