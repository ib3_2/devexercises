package pr1.uebung03;

import static pr.MakeItSimple.*;

public class Dividers {

	public static void main(String[] args) {
		// Variablen deklariert (initialisiert). Teiler = 1 weil es der Teiler
		// von jeder natürlichen Zahl ist
		int teiler = 1;
		int benutzerzahl;

		// Eingabe Aufforderung
		print("Bitte geben sie eine natürliche Zahl ein : ");

		// Die Eingabe wird in die Variable gespeichert
		benutzerzahl = readInt();

		// Zahl wird geprüft ob sie ein natürliche Zahl ist , und nicht kleiner
		// als 1 ist
		if (benutzerzahl > 0) {
			print("Die Teiler sind : ");

			// Bedingung, dass die Schleife solange läuft, bis die Zahlen bis
			// zur Benutzerzahl geprüft werden

			int a = 0;
			while (benutzerzahl > teiler) {
				// Zahl ist ein Teiler, wenn der Rest bei der Division 0 ist.
				// Teiler wird hochgezählt, um beim nächsten Schleifenlauf die
				// nächst höhere Zahl zu prüfen
				if ((benutzerzahl % teiler) == 0) {
					a = teiler;
					teiler++;
				} else {
					teiler++;
				}

			}

			print(a);
		} else {
			println("Eingabe ungültig");
		}
	}
}
