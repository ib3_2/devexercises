package pr1.uebung03;

import static pr.MakeItSimple.*;

public class PalindromeNumber {

	public static void main(String[] args) {
		/*
		 * Variablen werden deklariert und teilweise initialisiert. Teiler dient
		 * zum herausfinden, wie groß die eingegebende Zahl ist und um die
		 * einzelnen Zahlen zu extrahieren.
		 */
		int benutzerZahl;
		int teiler = 1;
		int index = 0;
		int zaehler = 1;
		int[] palindrome;

		println("Geben Sie eine Zahl ein die auf ein Palindrom geprüft wird. ");
		benutzerZahl = readInt();

		// Benutzerzahl wird nachdem möglichen vorgegebenden Randbereichen
		// abgefragt.
		if(benutzerZahl % 2 != 0){
		if (benutzerZahl >= 0 && benutzerZahl <= 2000000000) {

			/*
			 * Diese Schleife dient zum Herausrechnen, wie viele Elemente das
			 * Array besitzen wird. Dies wird im "zaehler" hochgerechnet. Der
			 * Teiler wird so hochmultipliziert, dass es die 1. Stelle später
			 * herausextrahiert.
			 */
			while (benutzerZahl / teiler >= 10) {
				teiler = teiler * 10;
				zaehler++;
			}
		}
			// Das Array "palindrome" bekommt die Länge durch die Variable
			// "zaehler" eingesetzt
			palindrome = new int[zaehler];

			/*
			 * Die Schleife dient dazu, um alle Zahlen von der gesamten
			 * benutzerzahl zu trennen, in dem es mit dem höchsten Teiler mit
			 * der Benutzerzahl teilt und dann in das Array speichert. Als
			 * nächstes wird von der Benutzerzahl die höchste Zahl abgezogen und
			 * der Teiler mit 10 dividiert, um den Prozess zu widerholen, bis
			 * die Zahl nur noch aus einem Einer besteht.
			 */

			while (zaehler > index) {
				palindrome[index] = benutzerZahl / teiler;
				benutzerZahl = benutzerZahl % teiler;
				teiler = teiler / 10;
				index++;
			}

			// Index wird zurückgesetzt, um wieder von vorne bei der nächsten
			// Schleife zu zählen.
			index = 0;

			/*
			 * Schleife dient zum checken und zum ausgeben vom Zahlenwert, wenn
			 * es ein Palendrom ist Es wird gecheckt, in dem vom höchsten
			 * Index(zaehler -1 ) und vom niedrigsten Index im Array die Zahlen
			 * miteinander überprüft werden. Sobald einmal eine Ungleichheit
			 * auftritt, wird die Ausgabe kein Palendrom getätigt und
			 * "durch zaehler = -1" die Schleife vorzeitig beendet.
			 */
			while (index < zaehler) {
				if (palindrome[index] == palindrome[zaehler - 1]) {
					index++;
					zaehler--;
					if (zaehler - 1 < index) {
						println("Die Zahl ist ein Palindrom");
					}
				} else {
					println("kein Palindrom");
					zaehler = -1;
				}
			}
		} else {
			println("ungültige Eingabe");
		}
	}
}
