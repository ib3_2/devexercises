package pr1.uebung03;

import static pr.MakeItSimple.*;

public class DividersArray {

	public static void main(String[] args) {
		/*
		 * Variablen werden deklariert. Der "teiler" Array besitzt, wie von der
		 * Aufgabe vorgegeben, 35 Elemente. "zaehler" dient als Zähler für den
		 * Index. Im Index 0 vom Teiler wird der erste Teiler 1 gespeichert
		 */
		int benutzerzahl;
		int[] teiler = new int[35];
		int zaehler = 0;
		teiler[0] = 1;

		print("Bitte geben sie eine natürliche Zahl ein : ");

		benutzerzahl = readInt();

		// Zahl wird überprüft, ob sie eine natürliche Zahl und nicht
		// kleiner als 1 ist.
		if (benutzerzahl > 0) {

			// Solange die Benutzerzahl nicht erreicht ist wird nach Zahlen
			// gesucht, die ein möglicher Teiler sein können.
			// Die Teiler werden im Array gespeichert und die Variable "zaehler"
			// setzt den Index fest, der hochgezählt wird sobald ein Teiler
			// gefunden wurde, um es an dieser Stelle im Array zu speichern.
			while (benutzerzahl > teiler[zaehler]) {
				if ((benutzerzahl % teiler[zaehler]) == 0) {
					zaehler++;
					teiler[zaehler] = teiler[zaehler - 1] + 1;
				} else {
					teiler[zaehler]++;
				}
			}

			// "zaehler" wird wieder auf 0 gesetzt, um in der Schleife alle
			// Ergebnisse von
			// vorne anzugeben.
			zaehler = 0;

			print("Die Teiler sind : ");

			// Bedingung, dass die Nullen im Array nicht ausgegeben werden und
			// die maximale Länge des Arrays nicht überschritten wird bzw. am
			// Ende des Arrays die while-Schleife endet.
			while (teiler[zaehler] != 0 && zaehler < 35) {
				print(teiler[zaehler] + " ");
				zaehler++;
			}
		} else {
			println("Eingabe ungültig");
		}
	}
}
