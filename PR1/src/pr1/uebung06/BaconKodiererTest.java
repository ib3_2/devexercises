package pr1.uebung06;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaconKodiererTest {

	BaconKodiererImpl kodierer = new BaconKodiererImpl();

	@Test
	public void reinigeNachricht() {
		assertEquals("HALLOSCHOENEWELT", BaconKodiererImpl.reinigeNachricht("Hallo, schöne Welt!"));
	}

	@Test
	public void kodiereNachricht() {
		assertEquals("kkgggkkkkkkgkgkkgkgkkggkggkgkkkkgkkkgkgkgkkgk", BaconKodiererImpl.kodiereNachricht("HALLOWELT"));
	}

	@Test
	public void dekodiereNachricht() {
		assertEquals("HALLOWELT",
				BaconKodiererImpl.dekodiereNachricht("kkgggkkkkkkgkgkkgkgkkggkggkgkkkkgkkkgkgkgkkgk"));
	}

	@Test
	public void versteckeNachricht() {
		String traegerMedium = "Mein Name ist Juan Sanchez Villa-Lobos Ramirez, "
				+ "oberster Metallurge am Hofe König Karl V. von Spanien; "
				+ "ich wurde 896 vor Christus in Ägypten geboren und " + "bin unsterblich seit 846 vor Christus.";
		String steganogramm = "MeiN nAme ist jUan saNcHez ViLla-LoboS RamIreZ, "
				+ "ObERstEr meTAllURgE aM Hofe KÖNig karl V. von SpanIen; "
				+ "ICh wURDE 896 vor christuS iN ÄgyptEn geboreN UNd " + "BIn unsTERbLIcH seIt 846 Vor Christus.";
		assertEquals(steganogramm,
				BaconKodiererImpl.versteckeNachricht("Treffen uns um drei Uhr am Bahnhof!", traegerMedium));
	}

	@Test
	public void zeigeNachricht() {
		String steganogramm = "es IST trauriG zU deNkEn, dIE nATuR spriCht uNd KeIneR hört zu.";
		String entschlüsselterText = BaconKodiererImpl.zeigeNachricht(steganogramm);
		assertTrue(entschlüsselterText.startsWith("HALLOWELT"));
	}

	@Test
	public void setzeStellenDerGroßBuchstabenAufTrue() {
		String traegerMedium = "abce fghij cdfjöü,";
		String binaerCode = "kkggkggkkkkkgkg";

		boolean[] sindGroßbuchstaben = BaconKodiererImpl.setzeStelleDerGroßbuchstabenAufTrue(traegerMedium, binaerCode);

		boolean[] expectedArray = { false, false, true, true, false, false, true, true, false, false, false, false,
				false, false, true, false, true, false };

		for (int i = 0; i < expectedArray.length; i++) {
			assertEquals(expectedArray[i], sindGroßbuchstaben[i]);
		}
	}

	@Test
	public void setzeSteganogrammZusammen() {
		String traegerMedium = "Hallo, du wunderschöne Welt!";
		String binaerCode = "kkgggkkkkkgkkgggkkkg";

		String steganogramm = BaconKodiererImpl.setzeSteganogrammZusammen(traegerMedium, binaerCode);

		String exceptedString = "haLLO, du wunDerSCHöne Welt!";

		assertEquals(exceptedString, steganogramm);
	}

	@Test
	public void testAufÜberlängeTrue() {
		String traegerMedium = "Das ist viel zu kurz!";
		String binaerCode = "kkkggkkkkkkgkkkgkkkgkkgk";

		assertTrue(BaconKodiererImpl.testeAufÜberlänge(traegerMedium, binaerCode));
	}

	@Test
	public void testAufÜberlängeFalse() {
		String traegerMedium = "Das ist lang genug!";
		String binaerCode = "kkkkggkkgkgkkgk";

		assertFalse(BaconKodiererImpl.testeAufÜberlänge(traegerMedium, binaerCode));
	}

	@Test
	public void ersetzeUmlaute() {
		String gereinigteNachricht = "ÄÖÜß";

		String ersetzteUmlaute = BaconKodiererImpl.ersetzeUmlaute(gereinigteNachricht);

		String expectedString = "AEOEUESS";

		assertEquals(expectedString, ersetzteUmlaute);
	}

	@Test
	public void testAufUngültigeZeichenTrue() {
		String gereinigteNachrichtMitUngültigemZeichen1 = "Abcdf";
		String gereinigteNachrichtMitUngültigemZeichen2 = "ABC,DFG";

		assertTrue(BaconKodiererImpl.testeAufUngültigeZeichen(gereinigteNachrichtMitUngültigemZeichen1));
		assertTrue(BaconKodiererImpl.testeAufUngültigeZeichen(gereinigteNachrichtMitUngültigemZeichen2));
	}

	@Test
	public void testAufUngültigeZeichenFalse() {
		String gereinigteNachricht = "ABCDF";

		assertFalse(BaconKodiererImpl.testeAufUngültigeZeichen(gereinigteNachricht));
	}

	@Test
	public void wandelSteganogrammInBinaer() {
		String steganogramm = "haLLO, du wunDerSCHöne Welt!";

		String binaerCode = BaconKodiererImpl.wandelSteganogrammInBinaer(steganogramm);

		String expectedString = "kkgggkkkkkgkkgggkkkgkkk";

		assertEquals(expectedString, binaerCode);

	}

	@Test
	public void setzeDekodierteNachrichtZusammen() {
		String dekodierteNachrichtErsterTeil = "HAU";
		String tempBinaerCode = "gkkkg";

		String dekodierteNachricht = dekodierteNachrichtErsterTeil
				+ BaconKodiererImpl.setzeDekodierteNachrichtZusammenMit(tempBinaerCode);

		String expectedString = "HAUS";

		assertEquals(expectedString, dekodierteNachricht);

	}

	@Test
	public void erstelleTemporaereBinaerCode() {
		String binaerCode = "kgkkgkg";
		String tempBinaerCode = "";

		tempBinaerCode = BaconKodiererImpl.erstelleMaximalFünfstelligenBinaerCode(binaerCode, 0);

		String expectedString = "kgkkg";

		assertEquals(expectedString, tempBinaerCode);
	}
}