package pr1.uebung06;

import static pr.MakeItSimple.*;

/* ERKLÄRUNG
In der Aufgabe wurden einige Regeln für einen "Clean Code" beachtet, 
deshalb sind die einzelnen Teilaufgaben (Methoden) wenn möglich noch mal 
unterteilt worden um möglichst kleine und auch testbare Methoden zu haben. 
Jede erstellte Methoden hat auch einen eigenen Test die alle positiv 
ausgefallen sind.
 */
public class BaconKodiererImpl {

	static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzäöüÄÖÜß".toCharArray();
	static String[] BINARY_ALPHABET = { "kkkkk", "kkkkg", "kkkgk", "kkkgg", "kkgkk", "kkgkg", "kkggk", "kkggg", "kgkkk",
			"kgkkk", "kgkkg", "kgkgk", "kgkgg", "kggkk", "kggkg", "kgggk", "kgggg", "gkkkk", "gkkkg", "gkkgk", "gkkgg",
			"gkkgg", "gkgkk", "gkgkg", "gkggk", "gkggg" };

	public static void main(String[] args) {
		// Start Teilaufgabe f)
		String nachricht = "Treffen uns um drei Uhr am Bahnhof!";
		String traegerMedium = "Mein Name ist Juan Sanchez Villa-Lobos Ramirez, oberster Metallurge am Hofe König Karl V. von Spanien;"
				+ "ich wurde 896 vor Christus in Ägypten geboren und bin unsterblich seit 846 vor Christus.";

		String steganogramm = versteckeNachricht(nachricht, traegerMedium);
		println(steganogramm);

		String entschlüsselteNachricht = zeigeNachricht(steganogramm);
		println(entschlüsselteNachricht);
		// Ende f)

		// Start Teilaufgabe g)
		String userEingabe = "";
		while (!userEingabe.equals("exit")) {

			gebeHauptmenüAus();
			userEingabe = readString().toLowerCase();

			switch (userEingabe) {
			case "botver":
				versteckeBotschaftUserInteraktion();
				break;
			case "botzeig":
				zeigeBotschaftUserInteraktion();
				break;
			case "demo":
				demonstriereBaconKodierer(nachricht, steganogramm);
				break;
			case "exit":
				println("Programm beendet.");
				break;

			default:
				println("Ihre Eingabe war ungültig. Probieren Sie Ihre Glück noch einmal.");
			}
		}

	}

	static void gebeHauptmenüAus() {
		println("\nHauptmenü");
		println("1. Botschaft verstecken. (Befehl: botver)");
		println("2. Botschaft sichtbar machen. (Befehl: botzeig)");
		println("3. Demo. (Befehl: demo)");
		println("4. Programm beenden. (Befehl: exit)");

		print("\nIhr Befehel bitte: ");
	}

	static void versteckeBotschaftUserInteraktion() {
		print("Bitte geben Sie eine Nachricht ein: ");
		String nachrichtUser = readString();
		print("Bitte geben Sie ein Trägermedium ein: ");
		String traegerMediumUser = readString();

		String steganogrammUser = versteckeNachricht(nachrichtUser, traegerMediumUser);

		println("Ihr Steganogramm = " + steganogrammUser);

		println("\nSie kehren in das Hauptmenü zurück.");
	}

	static void zeigeBotschaftUserInteraktion() {
		print("Bitte geben Sie Ihr Steganogramm ein: ");
		String steganogrammUser = readString();

		String entschlüsselteNachrichtUser = zeigeNachricht(steganogrammUser);
		println("Ihre Nachricht = " + entschlüsselteNachrichtUser);

		println("\nSie kehren in das Hauptmenü zurück.");
	}

	static void demonstriereBaconKodierer(String nachricht, String steganogramm) {
		println("In dieser Demonstration wird folgende Nachricht, \"Treffen uns um drei Uhr am Bahnhof!\",");
		println("im Trägermedium, \"Mein Name ist Juan Sanchez Villa-Lobos Ramirez, oberster Metallurge am Hofe König Karl V. von Spanien;");
		println("ich wurde 896 vor Christus in Ägypten geboren und bin unsterblich seit 846 vor Christus.\", versteckt.\n");

		println("Das Verstecken einer Nachricht: ");
		println("1. Schritt: Nachricht reinigen.");
		String gereinigteNachricht = reinigeNachricht(nachricht);
		println("Resultat nach der Reinigung: " + gereinigteNachricht + "\n");

		println("2. Schritt: Nachricht kodieren.");
		String binaerCode = kodiereNachricht(gereinigteNachricht);
		println("Resultat nach der Kodierung: " + binaerCode + "\n");

		println("3. Schritt: Binärcode auf Nachricht übertragen");
		println("Resultat nach Übertragung: \n" + steganogramm + "\n");

		println("\n Dechiffrierung einer Nachricht:");
		String binaerCode2 = wandelSteganogrammInBinaer(steganogramm);
		println("Resultat nach Umwandlung: " + binaerCode2 + "\n");

		println("2. Schritt: Binärcode dekodieren.");
		String dekodierteNachricht = dekodiereNachricht(binaerCode2);
		println("Resultat nach Dekodierung: " + dekodierteNachricht);
		println("Im 2. Schritt werden zwei zusätliche Zeichen ermittelt. Das liegt an der Überlänge des Trägermediums.");

		println("\nSie kehren in das Hauptmenü zurück.");
	}

	// Ende g)

	// Teilaufgabe d)
	static String versteckeNachricht(String nachricht, String traegerMedium) {
		String steganogramm = "";

		String gereinigteNachricht = reinigeNachricht(nachricht);
		String binaerCode = kodiereNachricht(gereinigteNachricht);

		boolean istZuLang = testeAufÜberlänge(traegerMedium, binaerCode);
		if (istZuLang) {
			throw new PRException("Träger Medium ist zu klein.");
		}

		steganogramm = setzeSteganogrammZusammen(traegerMedium, binaerCode);

		return steganogramm;
	}

	// Teilaufgabe a)
	static String reinigeNachricht(String nachricht) {

		char[] ungereinigteNachrichtArray = nachricht.toCharArray();
		String gereinigteNachricht = "";

		for (int i = 0; i < nachricht.length(); i++) {
			for (int j = 0; j < ALPHABET.length; j++) {
				if (ungereinigteNachrichtArray[i] == ALPHABET[j]) {
					gereinigteNachricht = gereinigteNachricht + ALPHABET[j];
					j = ALPHABET.length;
				}
			}
		}
		gereinigteNachricht = gereinigteNachricht.toUpperCase();
		gereinigteNachricht = ersetzeUmlaute(gereinigteNachricht);

		return gereinigteNachricht;
	}

	static String ersetzeUmlaute(String gereinigteNachricht) {
		gereinigteNachricht = gereinigteNachricht.replaceAll("Ä", "AE");
		gereinigteNachricht = gereinigteNachricht.replaceAll("Ö", "OE");
		gereinigteNachricht = gereinigteNachricht.replaceAll("Ü", "UE");
		gereinigteNachricht = gereinigteNachricht.replaceAll("ß", "SS");
		return gereinigteNachricht;
	}
	// Ende a)

	// Teilaufgabe b)
	static String kodiereNachricht(String gereinigteNachricht) {
		boolean istUngültigesZeichenVorhanden = testeAufUngültigeZeichen(gereinigteNachricht);
		if (istUngültigesZeichenVorhanden) {
			throw new PRException("Üngültiges Zeichen");
		}

		char[] gereinigteNachrichtArray = gereinigteNachricht.toCharArray();
		String kodierteNachricht = "";
		for (int i = 0; i < gereinigteNachricht.length(); i++) {
			for (int j = 0; j < ALPHABET.length; j++) {
				if (gereinigteNachrichtArray[i] == ALPHABET[j]) {
					kodierteNachricht = kodierteNachricht + BINARY_ALPHABET[j];
					j = ALPHABET.length;
				}
			}
		}

		return kodierteNachricht;
	}

	static boolean testeAufUngültigeZeichen(String gereinigteNachricht) {
		char[] gereinigteNachrichtArray = gereinigteNachricht.toCharArray();

		boolean istUngültigesZeichenVorhanden = false;

		int testCounter = 0;
		for (int i = 0; i < gereinigteNachricht.length(); i++) {
			for (int j = 0; j < ALPHABET.length; j++) {
				// Alle Großbuchstaben, da nur diese gültig
				if (gereinigteNachrichtArray[i] == ALPHABET[j] && (j < 26)) {
					j = ALPHABET.length;
					testCounter = testCounter + 1;
				}
			}
		}

		if (testCounter < gereinigteNachrichtArray.length) {
			istUngültigesZeichenVorhanden = true;
		}

		return istUngültigesZeichenVorhanden;
	}
	// Ende b)

	static boolean testeAufÜberlänge(String traegerMedium, String binaerCode) {
		boolean istZuLang = false;
		if (traegerMedium.length() < binaerCode.length()) {
			istZuLang = true;
		}

		return istZuLang;
	}

	static String setzeSteganogrammZusammen(String traegerMedium, String binaerCode) {

		boolean[] traegerMediumGroßbuchstaben = setzeStelleDerGroßbuchstabenAufTrue(traegerMedium, binaerCode);

		char[] traegerMediumCharArray = traegerMedium.toCharArray();

		String steganogramm = "";
		for (int i = 0; i < traegerMediumCharArray.length; i++) {
			if (traegerMediumGroßbuchstaben[i]) {
				String großBuchstabe = ("" + traegerMediumCharArray[i]).toUpperCase();
				steganogramm = steganogramm + großBuchstabe;
			} else if (i < binaerCode.length()) {
				String kleinBuchstabe = ("" + traegerMediumCharArray[i]).toLowerCase(); // ***
				steganogramm = steganogramm + kleinBuchstabe;
			} else {
				steganogramm = steganogramm + traegerMediumCharArray[i];
			}
		}
		return steganogramm;
	}

	static boolean[] setzeStelleDerGroßbuchstabenAufTrue(String traegerMedium, String binaerCode) {
		char[] traegerMediumCharArray = traegerMedium.toCharArray();
		char[] binaerCodeCharArray = binaerCode.toCharArray();

		boolean[] traegerMediumGroßbuchstaben = new boolean[traegerMedium.length()];

		// Setze alle Stellen im boolischen Array auf true, die später als
		// Großbuchstabe wieder hinzugefügt werden müssen
		int binaerCodeIndex = 0;
		for (int i = 0; i < traegerMediumCharArray.length; i++) {
			for (int j = 0; j < ALPHABET.length; j++) {
				if (traegerMediumCharArray[i] == ALPHABET[j] && binaerCodeIndex < binaerCodeCharArray.length) {
					traegerMediumGroßbuchstaben[i] = binaerCodeCharArray[binaerCodeIndex] == 'g';
					j = ALPHABET.length;
					binaerCodeIndex++;
				}
			}
		}
		return traegerMediumGroßbuchstaben;
	}

	// Ende d)

	// Teilaufgabe e)
	static String zeigeNachricht(String steganogramm) {

		String binaerCode = wandelSteganogrammInBinaer(steganogramm);

		String nachricht = dekodiereNachricht(binaerCode);

		return nachricht;
	}

	static String wandelSteganogrammInBinaer(String steganogramm) {
		char[] steganogrammArray = steganogramm.toCharArray();
		String binaerCode = "";

		for (int i = 0; i < steganogrammArray.length; i++) {
			for (int j = 0; j < ALPHABET.length; j++) {
				if (steganogrammArray[i] == ALPHABET[j] && (j < 26 || (54 < j && j < 59))) { // Großbuchstaben
					binaerCode = binaerCode + "g";
					j = ALPHABET.length;
				} else if (steganogrammArray[i] == ALPHABET[j] && (25 < j && j < 55)) { // Kleinbuchstaben
					binaerCode = binaerCode + "k";
					j = ALPHABET.length;
				}
			}
		}

		return binaerCode;
	}
	// Ende e)

	// Teilaufgabe c)
	static String dekodiereNachricht(String binaerCode) {
		String dekodierteNachricht = "";
		String tempBinaerCode = "";

		// Der Iterator läuft in 5er Schritten, da jeweils 5 Binärstellen einen
		// Buchstaben darstellen
		for (int i = 0; i < binaerCode.length(); i = i + 5) {
			tempBinaerCode = erstelleMaximalFünfstelligenBinaerCode(binaerCode, i);

			dekodierteNachricht = dekodierteNachricht + setzeDekodierteNachrichtZusammenMit(tempBinaerCode);
		}

		return dekodierteNachricht;
	}

	static String erstelleMaximalFünfstelligenBinaerCode(String binaerCode, int iterator) {
		char[] binaerCodeArray = binaerCode.toCharArray();

		String tempBinaerCode = "";
		// Erstellt einen Binärcode der später zur Überprüfung dient.
		for (int j = iterator; j < iterator + 5; j++) {
			if (j < binaerCodeArray.length && tempBinaerCode.length() < 5) {
				tempBinaerCode = tempBinaerCode + binaerCodeArray[j];
			}
		}

		return tempBinaerCode;
	}

	static String setzeDekodierteNachrichtZusammenMit(String tempBinaerCode) {
		String dekodierteNachricht = "";
		// Wenn der temporäre Binäercode 5 stellig ist wird im BINARY_ALPHABET
		// nach diesem Binärcode gesucht, ist dieser vorhanden ist der Index
		// gleich dem Index in ALPHABET und ALPHABET[k] kann somit an die
		// dekodierteNachricht angehangen werden
		if (tempBinaerCode.length() % 5 == 0) {
			for (int k = 0; k < BINARY_ALPHABET.length; k++) {
				if (tempBinaerCode.equals(BINARY_ALPHABET[k])) {
					dekodierteNachricht = dekodierteNachricht + ALPHABET[k];
					k = BINARY_ALPHABET.length;
				}
			}
		}

		return dekodierteNachricht;
	}
	// Ende c)
}
