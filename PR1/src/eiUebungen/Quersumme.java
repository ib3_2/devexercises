package eiUebungen;

public class Quersumme {

	public static void main(String[] args) {
		int zahl = 123;

		System.out.println(quersumme(zahl));
	}

	static int quersumme(int zahl) {
		if (zahl < 10) {
			return zahl;
		} else {
			return zahl % 10 + quersumme(zahl / 10);
		}
	}
}
