package eiUebungen;

import static pr.MakeItSimple.*;

public class Multiplikation {

	public static void main(String[] args) {
		println(multiplikationRecursive(-3, 4));
		println(multiplikationIterativ(3, 4));
	}

	static int multiplikationRecursive(int a, int b) {
		if (b > 1) {
			return a + multiplikationRecursive(a, b - 1);
		} else {
			return a;
		}
	}
	
	static int multiplikationIterativ(int a, int b){
		int result = 0;
		
		while(b>0){
			result = result + a;
			b = b -1;
		}
		
		return result;
	}
}
