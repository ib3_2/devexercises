package eiUebungen;

public class GlasMitSüßigkeiten {

	public static void main(String[] args) {
		int lakritz = 0;
		int gummibaerchen = 1;

		int x = 0;
		int y = 0;
		int glas = 9;

		while (glas > 0) {
			glas = glas - 2;

			double xX = Math.random();
			double yY = Math.random();

			if (xX > 0.5) {
				x = x + 1;
			}
			if (yY > 0.5) {
				y = y + 1;
			}

			if (x == gummibaerchen && y == gummibaerchen) {
				glas = glas + 1;

				x = 0;
				y = 0;
			}

			if (x == gummibaerchen && y == lakritz || x == lakritz && y == gummibaerchen) {
				glas = glas + 1;

				x = 0;
				y = 0;
			}

			System.out.print(glas);
		}
	}

}
