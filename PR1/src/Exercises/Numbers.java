package Exercises;

import static pr.MakeItSimple.*;

public class Numbers {

	public static void main(String[] args) {
		Numbers numbers = new Numbers();

		int uglyNumber = 249;
		boolean isUglyNumber = numbers.isUglyNumber(uglyNumber);

		println(uglyNumber + " is ugly number: " + isUglyNumber + "\n");

		int abundant = 0;
		int deficient = 1;
		int perfect = 2;

		int[] abundantNumbers = numbers.getNumbersWithProperty(abundant, 10000);
		int[] deficientNumbers = numbers.getNumbersWithProperty(deficient, 10000);

		for (int i = 0; i < 1000; i++) {
			if (numbers.isKaprekarNumber(i)) {
				print(i + " ");
			}
		}
	}

	boolean isUglyNumber(int n) {
		boolean isUglyNumber = true;

		while (n != 1 && isUglyNumber) {
			if (n % 5 == 0) {
				n /= 5;
			} else if (n % 3 == 0) {
				n /= 3;
			} else if (n % 2 == 0) {
				n /= 2;
			} else {
				isUglyNumber = false;
			}
		}

		return isUglyNumber;

	}

	int[] getNumbersWithProperty(int chooser, int n) {
		int arrayLength = getArrayLength(chooser, n);

		int[] numbersWithPropertyX = new int[arrayLength];
		int arrayIndexer = 0;
		for (int i = 2; i < n; i++) {
			int divisor = 0;
			for (int j = 1; j < i; j++) {
				if (i % j == 0) {
					divisor = divisor + j;
				}
			}

			if (chooser == 0 && divisor > i && arrayIndexer < numbersWithPropertyX.length) {
				numbersWithPropertyX[arrayIndexer] = i;
				arrayIndexer++;
			} else if (chooser == 1 && divisor < i && arrayIndexer < numbersWithPropertyX.length) {
				numbersWithPropertyX[arrayIndexer] = i;
				arrayIndexer++;
			} else if (chooser == 2 && divisor == i && arrayIndexer < numbersWithPropertyX.length) {
				numbersWithPropertyX[arrayIndexer] = i;
				arrayIndexer++;
			}
		}

		return numbersWithPropertyX;
	}

	private int getArrayLength(int chooser, int n) {
		int arrayLength = 0;
		for (int i = 2; i < n; i++) {
			int divisor = 0;
			for (int j = 1; j < i; j++) {
				if (i % j == 0) {
					divisor = divisor + j;
				}
			}
			if (chooser == 0 && divisor > i) {
				arrayLength++;
			} else if (chooser == 1 && divisor < i) {
				arrayLength++;
			} else if (chooser == 2 && divisor == i) {
				arrayLength++;
			}
		}
		return arrayLength;
	}

	boolean isKaprekarNumber(int n) {
		boolean isKaprekarNumber = true;
		int nPow2 = n * n;

		int arrayLength = 0;
		int tempPow2 = nPow2;
		while (tempPow2 != 0) {
			tempPow2 = tempPow2 / 10;
			arrayLength++;
		}

		int[] tempArray = new int[arrayLength];
		while (nPow2 != 0) {
			arrayLength--;
			tempArray[arrayLength] = nPow2 % 10;
			nPow2 = nPow2 / 10;
		}

		int tempResult = 0;
		if (tempArray.length % 2 == 0) {
			tempResult = getSumOfEvenLength(tempArray);
		} else {
			tempResult = getSumOfOddLength(tempArray);
		}

		if (tempResult == n) {
			return isKaprekarNumber;
		} else {
			return false;
		}

	}

	private int getSumOfEvenLength(int[] tempArray) {
		int tempResult = 0;

		if (tempArray.length != 2) {
			int tempNum1 = 0;
			int tempNum2 = 0;

			int multiplier = 1;
			for (int j = 1; j < tempArray.length / 2 + 1; j++) {
				tempNum1 = tempNum1 + tempArray[tempArray.length / 2 - j] * multiplier;
				multiplier *= 10;
			}

			multiplier = 1;
			for (int i = 1; i < tempArray.length / 2 + 1; i++) {
				tempNum2 = tempNum2 + tempArray[tempArray.length - i] * multiplier;
				multiplier *= 10;
			}

			tempResult = tempNum1 + tempNum2;

		} else {
			tempResult = tempArray[0] + tempArray[1];
		}

		return tempResult;
	}

	private int getSumOfOddLength(int[] tempArray) {
		int tempResult = 0;

		if (tempArray.length != 1) {
			int tempNum1 = 0;
			int tempNum2 = 0;

			int multiplier = 1;
			for (int i = 1; i < tempArray.length / 2 + 1; i++) {
				tempNum1 = tempNum1 + tempArray[tempArray.length / 2 - i] * multiplier;
				multiplier *= 10;
			}

			multiplier = 1;
			for (int j = 1; j <= tempArray.length / 2 + 1; j++) {
				tempNum2 = tempNum2 + tempArray[tempArray.length - j] * multiplier;
				multiplier *= 10;
			}

			tempResult = tempNum1 + tempNum2;

		} else {
			tempResult = 1;
		}

		return tempResult;
	}

}
